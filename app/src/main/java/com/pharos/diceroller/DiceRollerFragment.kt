package com.pharos.diceroller

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.*
import androidx.fragment.app.Fragment
import com.pharos.diceroller.databinding.FragmentDiceRollerBinding
import com.pharos.diceroller.databinding.ViewPlayerBinding
import kotlinx.coroutines.*

class DiceRollerFragment : Fragment() {
    private lateinit var binding: FragmentDiceRollerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDiceRollerBinding.inflate(inflater, container, false)

        val viewPlayerList = listOf(
            binding.playerView1,
            binding.playerView2,
            binding.playerView3,
            binding.playerView4
        )

        val drawableList = listOf(
            R.drawable.dice1,
            R.drawable.dice2,
            R.drawable.dice3,
            R.drawable.dice4,
            R.drawable.dice5,
            R.drawable.dice6
        )

        viewPlayerList.setPlayerNames("Player Name")

        binding.startButton.setOnClickListener {
            MainScope().launch {
                val diceAsyncList = viewPlayerList.map {it.diceImage}.rollDices(this, drawableList).awaitAll()

                val max = diceAsyncList.maxOf { it }
                val pos = diceAsyncList.indexOf(max)

                binding.playerViewWinner.diceImage.setImageResource(drawableList[max])
                binding.playerViewWinner.playerName.text = viewPlayerList[pos].playerName.text
            }
        }

        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun List<ViewPlayerBinding>.setPlayerNames(name: String) {
        this.forEachIndexed { i, it -> it.playerName.text = "$name ${i.inc()}" }
    }

    private suspend fun List<ImageView>.rollDices(
        coroutineScope: CoroutineScope,
        drawableList: List<Int>
    ) = this.map { coroutineScope.async { it.rollDice(drawableList) } }

    private suspend fun ImageView.rollDice(drawableList: List<Int>): Int {
        val job = this.rollDiceAnimation(drawableList)

        delay((1000L..2000L).random())
        job.cancelAndJoin()

        val randomDice = drawableList.random()

        this@rollDice.setImageResource(randomDice)

        return drawableList.indexOf(randomDice)
    }

    private suspend fun ImageView.rollDiceAnimation(drawableList: List<Int>) = MainScope().launch {
        while (true) {
            this@rollDiceAnimation.setImageResource(drawableList.random())
            delay(100)
        }
    }
}
